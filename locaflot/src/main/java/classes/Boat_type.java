package classes;

import java.lang.String;
import java.math.BigDecimal;

/**
 * Definition of the boat_type class, as defined in the BDD
 *  with the atrributes in english 
 * @author Victor LECOMTE and VERRIER Dorian
 */
public class Boat_type {
	//Attributes
	private String code;
	private String name;
	private int nb_place;
	private BigDecimal half_hour_price;
	private BigDecimal half_hour;
	private BigDecimal half_day_price;
	private BigDecimal half_day;
	
	
	// Accessors (getter)
	/**
     * Allows the recovery of the code 
     * @return String representing the code
     */
	public String getCode() {
		return code;
	}
	
	/**
     * Allows the recovery of the name 
     * @return String representing the name
     */
	public String getName() {
		return name;
	}
	
	/**
     * Allows the recovery of the nb_place 
     * @return Integer representing the nb_place
     */
	public int getNb_place() {
		return nb_place;
	}
	
	/**
     * Allows the recovery of the half_hour_price 
     * @return BigDecimal representing the half_hour_price
     */
	public BigDecimal getHalf_hour_price() {
		return half_hour_price;
	}
	
	/**
     * Allows the recovery of the half_hour
     * @return BigDecimal representing the half_hour
     */
	public BigDecimal getHalf_hour() {
		return half_hour;
	}
	
	/**
     * Allows the recovery of the half_day_price 
     * @return BigDecimal representing the half_day_price
     */
	public BigDecimal getHalf_day_price() {
		return half_day_price;
	}
	
	/**
     * Allows the recovery of the half_day 
     * @return BigDecimal representing the half_day
     */
	public BigDecimal getHalf_day() {
		return half_day;
	}

	// Mutators (setter)
	/**
	 * Allows the modification of the code  
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * Allows the modification of the name  
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Allows the modification of nb_place  
	 * @param nb_place
	 */
	public void setNb_place(int nb_place) {
		this.nb_place = nb_place;
	}
	
	/**
	 * Allows the modification of the half_hour_price  
	 * @param half_hour_price
	 */
	public void setHalf_hour_price(BigDecimal half_hour_price) {
		this.half_hour_price = half_hour_price;
	}

	/**
	 * Allows the modification of the half_hour
	 * @param half_hour
	 */
	public void setHalf_hour(BigDecimal half_hour) {
		this.half_hour = half_hour;
	}

	/**
	 * Allows the modification of the half_day_price
	 * @param half_day_price
	 */
	public void setHalf_day_price(BigDecimal half_day_price) {
		this.half_day_price = half_day_price;
	}

	/**
	 * Allows the modification of the half_day
	 * @param half_day
	 */
	public void setHalf_day(BigDecimal half_day) {
		this.half_day = half_day;
	}
	
	// Default constructor
	/**                                                                                                                                                        
     * Default constructor                                                                                                     
     */
    public Boat_type() {
    	super();
    	code = "";
    	name = "";
    	nb_place = 0;
    	half_hour_price = new BigDecimal(0);
    	half_hour = new BigDecimal(0);
    	half_day_price = new BigDecimal(0);
    	half_day = new BigDecimal(0);


    }
    
    // Constructor with parameters
    /**
     * Constructors with differents parameters
     * @param code the code of a boat_type
     * @param name the name of a boat_type
     * @param nb_place the nb_place of a boat_type
     * @param half_hour_price the half_hour_price of a boat_type
     * @param half_hour the half_hour of a boat_type
     * @param half_day_price the half_day_price of a boat_type
     * @param half_day the half_day of a boat_type
     */
  	public Boat_type(String code, String name, int nb_place, BigDecimal half_hour_price, BigDecimal half_hour, BigDecimal half_day_price, BigDecimal half_day) {
  		super();
  		this.code = code;
  		this.name = name;
  		this.nb_place = nb_place;
  		this.half_hour_price = half_hour_price;
  		this.half_hour = half_hour;
  		this.half_day_price = half_day_price;
  		this.half_day = half_day;
  	}

     // Methods 
  	/**
 	 *  Allows the display of all parameters (attributes)
 	 */
  	@Override
	public String toString() {
		return "Le code du type de l'embarcation est : " + code
				+ " son nom est : " + name 
				+ " son nombre de place est : " + nb_place 
				+ " le prix demi heure est : " + half_hour_price 
				+ " le pris de l'heure est :" + half_hour 
			    + " le prix de la demi journée est : " + half_day_price 
			    + " et le prix de la journée est : " + half_day;
	}
  	
}
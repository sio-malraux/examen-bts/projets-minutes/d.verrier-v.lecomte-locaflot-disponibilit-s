package vues;
import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import classes.*;
import controleurs.*;

import java.util.ArrayList;
import java.util.List;


import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class Vue_principale extends JFrame {

	private JPanel contentPane;
	protected ControleurPrincipal controleur;
	public JComboBox comboBox;
	public JTextArea AffichageListBoatAvailable;

	/**
	 * Create the frame.
	 * @param controleur 
	 */
	public Vue_principale(ControleurPrincipal controleur) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		comboBox = new JComboBox();
		comboBox.setActionCommand("visible");
		comboBox.addActionListener(controleur);
		comboBox.setBounds(26, -2, 318, 24);
		contentPane.add(comboBox, BorderLayout.NORTH);
		
		AffichageListBoatAvailable = new JTextArea();
		AffichageListBoatAvailable.setBounds(26, 53, 447, 108);
		contentPane.add(AffichageListBoatAvailable);
	}
	
	public void remplirComboBoat(List<Boat> lesBoats) {
		comboBox.addItem(new Boat());
		for (Boat c : lesBoats) {

			comboBox.addItem(c);
		}
	}
	
public void affichageLicencie(List<Boat> searchBoatAvailable) {
		
	AffichageListBoatAvailable.setText("");
		for (Boat b : searchBoatAvailable) {
			
			AffichageListBoatAvailable.setText(AffichageListBoatAvailable.getText() 
					+ "\n" + b.getId() + " "
					+ "\n" + b.getColor() + " " 
			        + "\n" + b.isAvailable() + " "
			        + "\n" + b.getType());
			
		}
		
	}

}

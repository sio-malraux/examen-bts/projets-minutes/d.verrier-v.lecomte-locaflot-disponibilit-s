package classes;

import java.lang.String;

/**
 * Definition of the boat class, as defined in the BDD
 *  with the atrributes in english 
 * @author Victor LECOMTE and VERRIER Dorian
 */
public class Boat {
	//Attributes
	private String id;
	private String color;
	private boolean available;
	private String type;
	
	// Accessors (getters)
	/**
     * Allows the recovery of the id 
     * @return String representing the id
     */
    public String getId() {
		return id;
	}
    
    /**
     * Allows the recovery of the color 
     * @return String representing the color
     */
    public String getColor() {
		return color;
	}
    
    /**
     * Allows the recovery of the available 
     * @return Boolean representing the available
     */
    public boolean isAvailable() {
		return available;
	}
    
    /**
     * Allows the recovery of the type 
     * @return String representing the type
     */
    public String getType() {
		return type;
	}

    // Mutators (setters)

    /**
     * Allows the modification of the id  
     * @param id
     */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Allows the modification of the color  
	 * @param color
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * Allows the modification of the available  
	 * @param available
	 */
	public void setAvailable(boolean available) {
		this.available = available;
	}

	/**
	 * Allows the modification of the type  
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	// Default constructor 
	/**                                                                                                                                                        
     * Default constructor                                                                                                     
     */
	public Boat() {
    	super();
    	id = "";
    	color = "";
    	available = true;
    	type = "";
    }

	// Constructor with parameters
	/**
	 * Constructors with differents parameters
	 * @param id the id of a boat
	 * @param color the color of a boat
	 * @param available the available of a boat
	 * @param type the type of a boat
	 */
 	public Boat(String id, String color, boolean available, String type) {
 		super();
 		this.id = id;
 		this.color = color;
 		this.available = available;
 		this.type = type;
 	}

    // Methods 
 	/**
 	 *  Allows the display of all parameters (attributes)
 	 */
 	@Override
	public String toString() {
		return "L'id de l'embarcation est : " + id 
				+ " puis sa couleur est : " + color 
				+ " ensuite sa disponibilité est à " + available
				+ " et pour finir le type est : " + type; 
	}
 	
}

package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Definition of connection to postgreSQL database
 * @author Victor LECOMTE and VERRIER Dorian
 */
public class ConnexionPostgreSql {

	private static Connection connect;
	 private static String url = "jdbc:postgresql://localhost:1999/v.lecomte";
		private static String user = "v.lecomte";
		private static String passwd = "P@ssword";
	/**
	 * 	Allows connection to the database
	 * @return connect
	 */
	public static Connection getInstance(){
		Connection connect = null;
		
		if(connect == null){
		try {
		connect = DriverManager.getConnection(url, user, passwd);
		} catch (SQLException e) {
		e.printStackTrace();
		}
		}
		return connect;
		}
}


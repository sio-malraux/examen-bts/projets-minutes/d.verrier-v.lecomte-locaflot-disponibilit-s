SELECT te.nom,e.disponible,e.id, cl.date, cl.heure_debut, cl.heure_fin,te.nb_place,e.couleur
FROM "locaflot_dispo".type_embarcation te 
LEFT JOIN "locaflot_dispo".embarcation e ON e.type = te.code
LEFT JOIN "locaflot_dispo".louer l ON l.id_embarcation = e.id
LEFT JOIN "locaflot_dispo".contrat_location cl ON cl.id = l.id_contrat
WHERE e.disponible = true;

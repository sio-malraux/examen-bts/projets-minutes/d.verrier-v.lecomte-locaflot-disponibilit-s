package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.*;

/**
* Definiton of class rentDAO
* @author Victor LECOMTE and VERRIER Dorian
*
*/
public abstract class RentDAO{
	
	public Connection connect = ConnexionPostgreSql.getInstance();
	
	/**
	 * Definition of the recupAll() method of the class rentDAO
	 */
	public List<Rent> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Rent> listRent = new ArrayList<Rent>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"locaflot_dispo\".louer");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table embarcation
				Rent aLocation = new Rent();
				
				aLocation.setContract_id(curseur.getInt("id_contrat"));
				aLocation.setBoat_id(curseur.getString("id_embarcation"));
				aLocation.setPersons_id(curseur.getInt("nb_personnes"));
			
			
				listRent.add(aLocation);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listRent; 
	}
}
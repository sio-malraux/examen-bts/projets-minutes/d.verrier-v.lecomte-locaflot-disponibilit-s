package controleurs;

import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import vues.Vue_principale;

import classes.*;
import dao.*;

public class ControleurPrincipal implements ActionListener {
	
	//déclaration des objets Modele qui permettront d’obtenir ou de transmettre les données :
	    private Vue_principale vue;
		private java.util.List<Boat> lesBoats;
	    

	public void actionPerformed(ActionEvent e) {
		
				if (e.getActionCommand().equals("Quitter"))
				{
				Quitter(); // déconnection et arrêt de l’application
				}	
	}
	


public void Quitter()
{
	System.out.println("Au revoir et à bientôt");
	System.exit(0);
}

public void setVue(Vue_principale vue) {
	this.vue = vue;
	vue.remplirComboBoat(lesBoats);
	
}
}





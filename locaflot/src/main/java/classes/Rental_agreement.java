package classes;

import java.sql.Date;
import java.sql.Time;

/**
 * Definition of the rental_agreement class, as defined in the DB
 *  with the attributes in english 
 * @author Victor LECOMTE and VERRIER Dorian
 */
public class Rental_agreement {
	// Attributes
	private int id;
	private Date date;
	private Time start_time;
	private Time end_time;
	
	// Accessors (getters)
	/**
     * Allows the recovery of the id 
     * @return Integer representing the id
     */
	public int getId() {
		return id;
	}
	
	/**
     * Allows the recovery of the date 
     * @return Date representing the date
     */
	public Date getDate() {
		return date;
	}
	
	/**
     * Allows the recovery of the start_time 
     * @return Time representing the start_time
     */
	public Time getStart_time() {
		return start_time;
	}
	
	/**
     * Allows the recovery of the end_time 
     * @return Time representing the end_time
     */
	public Time getEnd_time() {
		return end_time;
	}
	
	// Mutators (setters)
	
	/**
     * Allows the modification of the id  
     * @param id
     */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
     * Allows the modification of the date 
     * @param date
     */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
     * Allows the modification of the start_time  
     * @param start_time
     */
	public void setStart_time(Time start_time) {
		this.start_time = start_time;
	}
	
	/**
     * Allows the modification of the end_time 
     * @param end_time
     */
	public void setEnd_time(Time end_time) {
		this.end_time = end_time;
	}

	// Default constructor
	/**
	 * Default constructor
	 */
    public Rental_agreement() {
    	super();
    	id = 0;
    	date = new java.sql.Date(2020-06-15);
    	start_time = new java.sql.Time(0);
    	end_time = new java.sql.Time(0);
    }

	// Constructor with parameters
    /**
     * Constructors with differents parameters
     * @param id the id of rental_agreement
     * @param date the date of rental_agreement
     * @param start_time the start_time of rental_agreement
     * @param end_time the end_time of rental_agreement
     */
	public Rental_agreement(int id, Date date, Time start_time, Time end_time) {
		super();
		this.id = id;
		this.date = date;
		this.start_time = start_time;
		this.end_time = end_time;
	}

	// Methods
	/**
	 * Allows the display of all parameters (attributes)
	 */
	@Override
	public String toString() {
		return "L'id du contrat de location est : " + id 
				+ " la date du contrat est : " + date 
				+ " l'heure de début c'est : " + start_time 
				+ " l'heure de fin c'est : " + end_time;
	}
	
}